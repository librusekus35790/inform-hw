#include "stdio.h"

int main(){
    float a, b, c;
    unsigned short int t = 0;

    printf("Введите 8 чисел: ");
    scanf("%f %f", &a, &b);

    for (short int i = 3; i<=8; i+=1){
        scanf("%f", &c);

        if (a<b && b>c)
            t += 1;
        
        a = b;
        b = c;
    }

    printf("\n%i", t);
}
