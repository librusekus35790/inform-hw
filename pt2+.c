//Дано 200 вещественных чисел. Определить, сколько из них больше своих соседей, т.е. предыдущего и последующего чисел.

#include <stdio.h>

int main(){
    float a, b;
    short int c = 0, l=0;

    printf("1) ");
    scanf("%f", &a);

    for (short int i = 2; i<=8; i+=1){
        printf("%i) ",i);
        scanf("%f", &b);

        if (a<b)
            l = 1;
        else {
            if(l){
                if (a>b)
                    c+=1;
            l=0;
            }
        }
        a = b;
    }

    printf("\n%i", c);
}