//убрать строку и столбец из матрице по наиб. элементу

#include "stdio.h"
#include "math.h"
#include "locale.h"

const unsigned short int m = 6;

int main() {
	unsigned short int n = 0, mj = 0, mi = 0, t = 1;

	float s[m][m];
	float maxn = 0;

	setlocale(0, "");

	printf("Введите порядок матрицы и матрицу:");

	scanf_s("%hu", &n);

	for (int i = 0; i < n; i += 1) {
		for (int j = 0; j < n; j += 1) {
			scanf_s("%f", &s[i][j]);
		}
	}

	if (n == 1) {
		t = 0;
		printf("%hu", t);
		return 0;
	}

	maxn = s[0][0];

	for (int i = 0; i < n; i += 1) {
		for (int j = 0; j < n; j += 1) {
			if (fabsf(s[i][j]) > fabsf(maxn)) {
				maxn = s[i][j];
				mi = i;
				mj = j;
			}
		}
	}

	for (int i = 0; i < n; i += 1) {
		for (int j = mj + 1; j < n; j += 1) {
			s[i][j - 1] = s[i][j];
		}
	}

	for (int j = 0; j < n; j += 1) {
		for (int i = mi + 1; i < n; i += 1) {
			s[i - 1][j] = s[i][j];
		}
	}

	n -= 1;
	printf("\n%hu\n", t);

	for (int i = 0; i < n; i += 1) {
		for (int j = 0; j < n; j += 1) {
			printf("%f\t", s[i][j]);
		}
		printf("\n");
	}
}
