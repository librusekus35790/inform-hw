//Найти все слова, перед которыми в последовательности стоят только мЕньшие слова, а после - только бОльшие.

#include "stdio.h"
#include "locale.h"
#include "string.h"

int main() {
	char sin[30][9], sout[30][9], t;
	unsigned char k;
	unsigned short int w = 0, wo = 0;

	setlocale(0, "");

	printf("Введите предложение: ");

	do {
		scanf_s("%8[^,.]%1c", sin[w], 9, &t, 1);
		w += 1;
	} while (t != '.');


	for (unsigned short int i = 1; i < w - 1; i += 1) {
		k = 1;
		for (unsigned short int l = 0; l < i; l += 1) {
			if (strcmp(sin[l], sin[i]) >= 0) {
				k = 0;
				break;
			}
		}

		if (k == 0) continue;

		for (unsigned short int r = i + 1; r < w; r += 1) {
			if (strcmp(sin[i], sin[r]) >= 0) {
				k = 0;
				break;
			}
		}
		if (k) {
			strcpy_s(sout[wo], sin[i]);
			wo += 1;
		}
	}


	printf("%hu\n", wo);

	for (unsigned short int i = 0; i < wo; i += 1)
		printf("%s ", sout[i]);

	return 0;
}
