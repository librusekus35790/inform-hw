//три точки треугольника

#include <stdio.h>
#include <math.h>
#include <locale>

const float precision = 0.01;

int main() {
	float x1, x2, x3, y1, y2, y3;
	float s, s1, s2, s3;
	float p, lena, lenb, lenc;

	setlocale(0, "");

	printf("\n Введите координаты первой точки(x1 y1): ");
	scanf_s("%f %f", &x1, &y1);

	printf("\n Введите координаты второй точки(x2 y2): ");
	scanf_s("%f %f", &x2, &y2);

	printf("\n Введите координаты третьей точки(x3 y3): ");
	scanf_s("%f %f", &x3, &y3);

	//Большой треугольник

	lena = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	lenb = sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));
	lenc = sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3));

	if (lenc == 0 || lena == 0 || lenb == 0) {
		printf("Не треугольник");
		return 0;
	}

	p = (lena + lenb + lenc) / 2;

	s = sqrt(p * (p - lena) * (p - lenb) * (p - lenc));

	//Маленький 1

	lena = sqrt((0 - x2) * (0 - x2) + (0 - y2) * (0 - y2));
	lenb = sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));
	lenc = sqrt((0 - x3) * (0 - x3) + (0 - y3) * (0 - y3));

	p = (lena + lenb + lenc) / 2;

	s1 = sqrt(p * (p - lena) * (p - lenb) * (p - lenc));

	//Маленький 2

	lena = sqrt((x1 - 0) * (x1 - 0) + (y1 - 0) * (y1 - 0));
	lenb = sqrt((x3 - 0) * (x3 - 0) + (y3 - 0) * (y3 - 0));
	lenc = sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3));

	p = (lena + lenb + lenc) / 2;

	s2 = sqrt(p * (p - lena) * (p - lenb) * (p - lenc));

	//Маленький 3

	lena = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	lenb = sqrt((0 - x2) * (0 - x2) + (0 - y2) * (0 - y2));
	lenc = sqrt((x1 - 0) * (x1 - 0) + (y1 - 0) * (y1 - 0));

	p = (lena + lenb + lenc) / 2;

	s3 = sqrt(p * (p - lena) * (p - lenb) * (p - lenc));

	if (s == 0) {
		printf("Не треугольник");
		return 0;
	}

	if (abs(s - (s1 + s2 + s3)) < precision) {
		printf("\nДа");
	}
	else printf("\nНет");

	return 0;
}
