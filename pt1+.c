//Даны координаты (целые от 1 до 8) двух полей шахматной доски. Определить, может ли конь за один ход перейти с одного из этих полей в другое.

#include "stdio.h"
#include "locale.h"
#include "math.h"

int main() {
	unsigned short int x1, y1, x2, y2, t;

	setlocale(0, "");
	printf("Введите координаты точек: ");
	scanf_s("%hu %hu %hu %hu", &x1, &y1, &x2, &y2);

	if (abs(x1 - x2) == 1 && abs(y1 - y2) == 2 || abs(x1 - x2) == 2 && abs(y1 - y2) == 1) {
		t = 1;
	}
	else t = 0;

	printf("%hu\n", t);
	return 0;
}
