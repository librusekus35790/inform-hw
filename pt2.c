//вычислить интегральный синус

#include <stdio.h>
#include <math.h>

int main() {
	int M = 0, k = 2;
	float x = 0, sum = 0, A = 0;

	printf("x, M = ");
	scanf_s("%f %d", &x, &M);

	A = x;
	sum = A;

	for (int n = 1; n < M; n += 1) {
		A = (-A * x * x) /
			((k + 1) * k);

		sum += A / (k + 1);
		k += 2;
	}

	printf("%f\n", sum);
}
