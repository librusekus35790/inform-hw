#include "stdio.h"
#include "math.h"
#include "string.h"

const unsigned short int maxn = 6;

void scansmatrix(float m[maxn][maxn], unsigned short int n){
for (unsigned short int i = 0; i<n; i+=1)
for (unsigned short int j = 0; j<n; j+=1){
scanf("%f", &m[i][j]);
}
}

void printsmatrix(float m[maxn][maxn], unsigned short int n){
for (unsigned short int i = 0; i<n; i+=1){
for (unsigned short int j = 0; j<n; j+=1){
printf("%f ", m[i][j]);
}
printf("\n");
}
}

float getmatrixnorm(float m[maxn][maxn], unsigned short int n){
float max = m[0][0];

for (unsigned short int i = 0; i<n; i+=1)
for (unsigned short int j = 0; j<n; j+=1){
if (fabsf(m[i][j])>max)
max = fabsf(m[i][j]);
}

return max;
}

int main(){
unsigned short int n;
float a[maxn][maxn], b[maxn][maxn], c[maxn][maxn],
a1, b1, c1;

printf("Введите размерность и три матрицы: ");

scanf("%hu", &n);

scansmatrix(a, n);
scansmatrix(b, n);
scansmatrix(c, n);

a1 = getmatrixnorm(a, n);
b1 = getmatrixnorm(b, n);
c1 = getmatrixnorm(c, n);

if (a1<b1 && a1<c1){
printsmatrix(a, n);
}
else if (b1 < c1 && b1 < a1){
printsmatrix(b, n);
}
else
printsmatrix(c, n);

return 0;
}
